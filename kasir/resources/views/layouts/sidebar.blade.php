<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('menu-utama') }}" class="brand-link text-sm">
        <img src="{{ asset('img/logo.jpg') }}" alt="" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">Kasir Parfume </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('menu-utama') }}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Menu Utama
                        </p>
                    </a>
                </li>
                @role('admin')
                    <li class="nav-header">Admin</li>
                    <li class="nav-item">
                            <i class="nav-icon fas fa-search"></i>
                            <p>Data Barang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('kategori') }}"class="nav-link">
                            <i class="nav-icon fas fa-cloud"></i>
                            <p>Kategori Barang</p>
                        </a>
                    </li>
                    <li class="nav-item">
                       <a href="{{ route('unit') }}" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p>
                                Satuan
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <!--<a href="" class="nav-link">-->
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Barang Masuk
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <!--<a href="" class="nav-link">-->
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Kasir
                            </p>
                        </a>
                    </li>
                @endrole
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
