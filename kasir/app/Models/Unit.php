<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;
    //protected $table = 'nama tabel yg tidak sesuai dengan penamaan'
    //protected $primaryKey = 'id';
    protected $guarded = [];//berlaku untuk semua atribut, kebalikan $fillable
}
