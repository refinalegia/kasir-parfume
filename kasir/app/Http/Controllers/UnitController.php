<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Unit;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $data = Unit::all();
            return datatables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                //$btn = '<a class="btn btn-info" href="' . route('kategori.edit', $data->id) . '"><i class="fas fa-eye"></i></a>';-->
                //return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('pages.unit.index', [
            'title' => 'Kasir Parfume | Halaman Satuan Barang'
        ]);
    }

    /**
     * Shoe the form for creating a new resource.
     *
     * @return Iluminate\Http\Response
     */
    public function create()
    {
        $model = new Unit;
        return view('pages.unit.index', [
            'title' => 'Kasir Parfume | Tambah Satuan Barang'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Unit;
        //$model->id = $request-> id;
        $model->name = $request->name;
        $model->created_by = $request->created_by;
        $model->save();
        return redirect()->route('unit')->with('success', "Data berhasil disimpan");
    }

    public function show($id)
    {

    }


}
