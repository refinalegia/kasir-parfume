<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    DashboardController, KategoriController, UnitController
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


    //Route::middleware('role:admin')->group(function (){
        //Route::resource('/product', ProductController::class);
        Route::get('/kategori', [KategoriController::class, 'index'])->name('kategori');
        Route::get('/satuan', [UnitController::class, 'index'])->name('unit');

   // });



    Route::middleware('role:cashier')->group(function (){
        //
    });
    Route::get('/menu-utama', [DashboardController::class, 'index'])->name('menu-utama');
